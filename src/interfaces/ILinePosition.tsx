export default interface ILinePosition {
    startTop: number;
    startLeft: number;
    endTop: number;
    endLeft: number;
    controlLeft1: number;
    controlTop1: number;
    controlLeft2: number;
    controlTop2: number;
}