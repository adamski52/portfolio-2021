import ILinePosition from "./ILinePosition";

export default interface ILine extends ILinePosition {
    color: string;
}