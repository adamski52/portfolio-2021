import React from "react";
import ILine from "../interfaces/ILine";
import "./Line.scss";

export interface ILineProps {
    line: ILine;
    lineThickness: number;
}

export interface ILineState {
    line: ILine;
    lineThickness: number;
}

export default class Line extends React.Component<ILineProps, ILineState> {
    constructor(props:ILineProps) {
        super(props);
        
        this.state = {
            line: props.line,
            lineThickness: props.lineThickness
        };
    }

    public render() {
        return (
            <path
                data-testid="svg-path"
                // d={`M ${this.state.line.startLeft} ${this.state.line.startTop} C ${this.state.line.controlLeft1} ${this.state.line.controlTop1} ${this.state.line.controlLeft2} ${this.state.line.controlTop2} ${this.state.line.endLeft} ${this.state.line.endTop}`}
                d={`M ${this.state.line.startLeft} ${this.state.line.startTop} ${this.state.line.endLeft} ${this.state.line.endTop}`}
                fill="none"
                stroke={this.state.line.color}
                strokeWidth={this.state.lineThickness}
            />
        );
    }

    public static getDerivedStateFromProps(props:ILineProps, state:ILineState)  {
        return {
            ...state,
            line: props.line,
            lineThickness: props.lineThickness
        };
    }
}