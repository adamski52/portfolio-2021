import React from "react";
import IItem from "../interfaces/IItem";
import ItemService from "../services/ItemsService";
import { ISkillRef } from "../services/SkillsService";
import Skill from "./Skill";

export interface ISkillsContainerProps {
    skills: ISkillRef[];
    activeItem: IItem;
    itemService: ItemService;
}

export interface ISkillsContainerState {
    skills: ISkillRef[];
    activeItem: IItem;
}

export default class SkillsContainer extends React.Component<ISkillsContainerProps, ISkillsContainerState> {
    constructor(props:ISkillsContainerProps) {
        super(props);
        
        this.state = {
            skills: props.skills,
            activeItem: props.activeItem
        };
    }

    public render() {
        return (
            <div className="skills-container text-center">
                {this.renderSkills()}
            </div>
        );
    }

    private renderSkills() {
        return this.state.skills.map((skill) => {
            return (
                <Skill skill={skill} key={skill.name} isActive={this.props.itemService.isSkillUsedInItem(this.state.activeItem, skill.name)} />
            );
        });
    }

    public static getDerivedStateFromProps(props:ISkillsContainerProps, state:ISkillsContainerState)  {
        return {
            ...state,
            skills: props.skills
        };
    }
}