import React from "react"
import { render, RenderResult } from "@testing-library/react"
import ILine from "../interfaces/ILine";
import Line from "./Line";

describe("Line", () => {
    let component:RenderResult;

    const line:ILine = {
        startLeft: 1,
        startTop: 2,
        endLeft: 3,
        endTop: 4,
        controlLeft1: 5,
        controlTop1: 6,
        controlLeft2: 7,
        controlTop2: 8,
        color: "#000"
    };

    it("should render the right svg syntax", () => {
        component = render(<Line lineThickness={5} line={line} />);

        let element = component.getByTestId("svg-path");
        expect(element).toBeInTheDocument();
        // expect(element).toHaveAttribute("d", "M 1 2 C 5 6 7 8 3 4");
        expect(element).toHaveAttribute("d", "M 1 2 3 4");
        expect(element).toHaveAttribute("stroke", "#000");
        expect(element).toHaveAttribute("stroke-width", "5");
    });
});
