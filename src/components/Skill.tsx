import React from "react";
import { ISkillRef } from "../services/SkillsService";
import "./Skill.scss";

export interface ISkillProps {
    skill: ISkillRef;
    isActive: boolean;
}

export interface ISkillState {
    skill: ISkillRef;
    isActive: boolean;
}

export default class Skill extends React.Component<ISkillProps, ISkillState> {
    constructor(props:ISkillProps) {
        super(props);
        
        this.state = {
            skill: props.skill,
            isActive: props.isActive
        };
    }

    public render() {
        return (
            <img ref={this.state.skill.ref} alt={this.state.skill.name} src={"img/icons/" + this.state.skill.name + ".png"} className={"skill " + this.getClassName()}/>
        );
    }

    private getClassName() {
        if(this.state.isActive) {
            return "active";
        }

        return "";
    }

    public static getDerivedStateFromProps(props:ISkillProps, state:ISkillState)  {
        return {
            ...state,
            skill: props.skill,
            isActive: props.isActive
        };
    }
}