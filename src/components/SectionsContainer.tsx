import React from "react";
import SectionButton from "./SectionButton";

export type TSection = "business" | "pleasure" | "info";

export interface ISectionsContainerProps {
    currentSection: TSection;
    onChangeSection: (section:TSection) => void;
}

export interface ISectionsContainerState {
    currentSection: TSection;
}

export default class SectionsContainer extends React.Component<ISectionsContainerProps, ISectionsContainerState> {
    constructor(props:ISectionsContainerProps) {
        super(props);
        
        this.state = {
            currentSection: props.currentSection
        };
    }

    public render() {
        return (
            <nav className="text-center">
                {this.renderButton("business")}
                {this.renderButton("pleasure")}
                {this.renderButton("info")}
            </nav>
        );
    }

    private renderButton(section:TSection) {
        return (
            <SectionButton onChangeSection={this.props.onChangeSection} section={section} isActive={this.state.currentSection === section} />
        );
    }

    public static getDerivedStateFromProps(props:ISectionsContainerProps, state:ISectionsContainerState)  {
        return {
            ...state,
            currentSection: props.currentSection
        };
    }
}