import React, { MouseEvent } from "react";

export type TSection = "business" | "pleasure" | "info";

export interface ISectionButtonProps {
    section: TSection;
    isActive: boolean;
    onChangeSection: (section:TSection) => void;
}

export interface ISectionButtonState {
    section: TSection;
    isActive: boolean;
}

export default class SectionButton extends React.Component<ISectionButtonProps, ISectionButtonState> {
    constructor(props:ISectionButtonProps) {
        super(props);
        
        this.state = {
            section: props.section,
            isActive: props.isActive
        };

        this.onChangeSection = this.onChangeSection.bind(this);
    }

    private onChangeSection(e:MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.onChangeSection(this.props.section);
    }

    private getClassName() {
        if(this.state.isActive) {
            return "active";
        }

        return "";
    }

    public render() {
        return (
            <button className={"btn " + this.getClassName()} onClick={this.onChangeSection}>{this.state.section}</button>
        );
    }

    public static getDerivedStateFromProps(props:ISectionButtonProps, state:ISectionButtonState)  {
        return {
            ...state,
            section: props.section,
            isActive: props.isActive
        };
    }
}