import React from "react";
import { IHat } from "../services/HatsService";
import "./HatsContainer.scss";

export interface IHatsContainerProps {
    hats: IHat[];
}

export interface IHatsContainerState {
    hats: IHat[];
}

export default class HatsContainer extends React.Component<IHatsContainerProps, IHatsContainerState> {
    constructor(props:IHatsContainerProps) {
        super(props);
        
        this.state = {
            hats: props.hats
        };
    }

    public render() {
        return (
            <div className="text-center">
                {this.renderHats()}
            </div>
        );
    }

    private getClassName(hat:IHat) {
        if(hat.isActive) {
            return "active";
        }

        return "";
    }

    private renderHats() {
        return this.state.hats.map((hat) => {
            return (
                <div key={hat.name} className={"hat hat-" + hat.name + " " + this.getClassName(hat)}>{hat.name}</div>
            );
        });
    }

    public static getDerivedStateFromProps(props:IHatsContainerProps, state:IHatsContainerState)  {
        return {
            ...state,
            hats: props.hats
        };
    }
}