import React from "react"
import { render, RenderResult } from "@testing-library/react"
import Skill from "./Skill";
import { ISkill } from "../services/SkillsService";

describe("Skill", () => {
    let component:RenderResult;

    const skill:ISkill = {
        name: "skill1",
        skill: 1
    };

    it("should render with the icon", () => {
        component = render(<Skill skill={skill} isActive={false} />);

        expect(component.getByAltText("skill1")).toBeInTheDocument();
    });

    it("should render with active class, if active", () => {
        component = render(<Skill skill={skill} isActive={true} />);

        expect(component.getByAltText("skill1")).toHaveClass("active");
    });

    it("should not render with active class, if not active", () => {
        component = render(<Skill skill={skill} isActive={false} />);

        expect(component.getByAltText("skill1")).not.toHaveClass("active");
    });
});
