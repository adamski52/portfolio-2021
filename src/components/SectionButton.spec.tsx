import React from "react"
import { render, RenderResult } from "@testing-library/react"
import SectionButton from "./SectionButton";

describe("SectionButton", () => {
    let component:RenderResult;
    let onChangeSection: jest.Mock<any, any>;

    beforeEach(() => {
        onChangeSection = jest.fn();
    });
    
    it("should render with the right section", () => {
        component = render(<SectionButton onChangeSection={onChangeSection} isActive={false} section={"business"} />);

        let element = component.getByText("business");
        expect(element).toBeInTheDocument();
    });

    it("should render with the active class, if active", () => {
        component = render(<SectionButton onChangeSection={onChangeSection} isActive={true} section={"business"} />);

        let element = component.getByText("business");
        expect(element).toBeInTheDocument();
        expect(element).toHaveClass("active");
    });

    it("should not render with the active class, if not active", () => {
        component = render(<SectionButton onChangeSection={onChangeSection} isActive={false} section={"business"} />);

        let element = component.getByText("business");
        expect(element).toBeInTheDocument();
        expect(element).not.toHaveClass("active");
    });

    it("should call onChangeSection onClick", () => {
        component = render(<SectionButton onChangeSection={onChangeSection} isActive={true} section={"business"} />);

        let element = component.getByText("business");
        element.click();
        expect(onChangeSection).toHaveBeenCalledWith("business");
    });
});
