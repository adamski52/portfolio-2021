import React from "react";
import ILine from "../interfaces/ILine";
import "./LinesContainer.scss";
import Line from "./Line";

export interface ILinesContainerProps {
    lines: ILine[];
    width: number;
    height: number;
    lineThickness: number;
}

export interface ILinesContainerState {
    lines: ILine[];
    width: number;
    height: number;
    lineThickness: number;
}

export default class LinesContainer extends React.Component<ILinesContainerProps, ILinesContainerState> {
    constructor(props:ILinesContainerProps) {
        super(props);
        
        this.state = {
            lines: props.lines,
            width: props.width,
            height: props.height,
            lineThickness: props.lineThickness
        };
    }

    public render() {
        return (
            <svg className="lines-container" data-testid="svg" viewBox={`0 0 ${this.state.width} ${this.state.height}`}>
                {this.renderLines()}
            </svg>
        );
    }

    
    private renderLines() {
        return this.state.lines.map((line) => {
            return (
                <Line line={line} lineThickness={this.state.lineThickness} />
            );
        });
    }

    public static getDerivedStateFromProps(props:ILinesContainerProps, state:ILinesContainerState)  {
        return {
            ...state,
            lines: props.lines,
            width: props.width,
            height: props.height,
            lineThickness: props.lineThickness
        };
    }
}