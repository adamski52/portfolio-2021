import React from "react"
import { render, RenderResult } from "@testing-library/react"
import ILine from "../interfaces/ILine";
import LinesContainer from "./LinesContainer";

describe("LinesContainer", () => {
    let component:RenderResult;

    const line1:ILine = {
        startLeft: 11,
        startTop: 12,
        endLeft: 13,
        endTop: 14,
        controlLeft1: 15,
        controlTop1: 16,
        controlLeft2: 17,
        controlTop2: 18,
        color: "#111"
    };

    const line2:ILine = {
        startLeft: 21,
        startTop: 22,
        endLeft: 23,
        endTop: 24,
        controlLeft1: 25,
        controlTop1: 26,
        controlLeft2: 27,
        controlTop2: 28,
        color: "#222"
    };

    const line3:ILine = {
        startLeft: 31,
        startTop: 32,
        endLeft: 33,
        endTop: 34,
        controlLeft1: 35,
        controlTop1: 36,
        controlLeft2: 37,
        controlTop2: 38,
        color: "#333"
    };

    const lines = [line1, line2, line3];

    const height = 200;
    const width = 100;

    it("should render with the right number of lines", () => {
        component = render(<LinesContainer lineThickness={5} width={width} height={height} lines={lines} />);

        let element = component.getByTestId("svg");
        expect(element).toBeInTheDocument();
        expect(element).toHaveAttribute("viewBox", "0 0 " + width + " " + height);

        expect(component.container.querySelectorAll("path").length).toEqual(3);
    });
});
