import React from "react"
import { render, RenderResult } from "@testing-library/react"
import App from "./App";
import SkillsService from "../services/SkillsService";
import IItem from "../interfaces/IItem";
import HatsService from "../services/HatsService";
import HistoryService from "../services/HistoryService";
import ItemService from "../services/ItemsService";
import LineService from "../services/LineService";
import { makeItem } from "../setupTests";

describe("App", () => {    
    const item1:IItem = makeItem("item1"),
          item2:IItem = makeItem("item2"),
          item3:IItem = makeItem("item3");

    const items = [item1, item2, item3];
    
    const hatsService = new HatsService();
    const skillsService = new SkillsService();
    const historyService = new HistoryService(skillsService, hatsService);
    const itemService = new ItemService(items);
    const lineService = new LineService(5);

    let component:RenderResult;
    let onChangeSection: jest.Mock<any, any>;

    beforeEach(() => {
        onChangeSection = jest.fn();
    });
    
    it("should render", () => {
        component = render(<App historyService={historyService} itemService={itemService} hatsService={hatsService} skillsService={skillsService} lineService={lineService} />);
    });

    it("should have the proper left and right side skills", () => {
        component = render(<App historyService={historyService} itemService={itemService} hatsService={hatsService} skillsService={skillsService} lineService={lineService} />);

        let skillsContainers = component.container.querySelectorAll(".skills-container")
        expect(skillsContainers[0].querySelectorAll("img").length).toEqual(36);
        expect(skillsContainers[1].querySelectorAll("img").length).toEqual(36);
    });
});
