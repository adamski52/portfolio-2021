import React from "react";
import IItem from "../interfaces/IItem";
import "./Item.scss";

export interface IItemProps {
    item: IItem;
    isActive: boolean;
}

export interface IItemState {
    item: IItem;
    isActive: boolean;
}

export default class ItemsContainer extends React.Component<IItemProps, IItemState> {
    constructor(props:IItemProps) {
        super(props);
        
        this.state = {
            item: props.item,
            isActive: props.isActive
        };
    }

    private getClassName() {
        if(this.state.isActive) {
            return "active";
        }

        return "";
    }

    public render() {
        return (
            <div className={"item " + this.getClassName()}>
                <h2>{this.state.item.name}</h2>
                <img src={this.state.item.thumbnailUrl} alt={this.state.item.name} />
            </div>
        );
    }

    public static getDerivedStateFromProps(props:IItemProps, state:IItemState)  {
        return {
            ...state,
            item: props.item,
            isActive: props.isActive
        };
    }
}