import React from "react"
import { render, RenderResult } from "@testing-library/react"
import SectionsContainer from "./SectionsContainer";

describe("SectionsContainer", () => {
    let component:RenderResult;
    let onChangeSection: jest.Mock<any, any>;

    beforeEach(() => {
        onChangeSection = jest.fn();
    });
    
    it("should render with the right sections", () => {
        component = render(<SectionsContainer onChangeSection={onChangeSection} currentSection={"business"} />);

        expect(component.getByText("business")).toBeInTheDocument();
        expect(component.getByText("pleasure")).toBeInTheDocument();
        expect(component.getByText("info")).toBeInTheDocument();
    });

    it("should render with business active, if so", () => {
        component = render(<SectionsContainer onChangeSection={onChangeSection} currentSection={"business"} />);
        expect(component.getByText("business")).toHaveClass("active")
        expect(component.getByText("pleasure")).not.toHaveClass("active");
        expect(component.getByText("info")).not.toHaveClass("active");
    });

    it("should render with pleasure active, if so", () => {
        component = render(<SectionsContainer onChangeSection={onChangeSection} currentSection={"pleasure"} />);
        expect(component.getByText("business")).not.toHaveClass("active")
        expect(component.getByText("pleasure")).toHaveClass("active");
        expect(component.getByText("info")).not.toHaveClass("active");
    });

    it("should render with info active, if so", () => {
        component = render(<SectionsContainer onChangeSection={onChangeSection} currentSection={"info"} />);
        expect(component.getByText("business")).not.toHaveClass("active")
        expect(component.getByText("pleasure")).not.toHaveClass("active");
        expect(component.getByText("info")).toHaveClass("active");
    });
});
