import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { IHat } from "../services/HatsService";
import HatsContainer from "./HatsContainer";

describe("HatsContainer", () => {
    let component:RenderResult;

    const hat1:IHat = {
        name: "hat1",
        isActive: true
    };

    const hat2:IHat = {
        name: "hat2",
        isActive: false
    };

    const hat3:IHat = {
        name: "hat3",
        isActive: true
    };

    const hats = [hat1, hat2, hat3];

    it("should render with the right hats", () => {
        component = render(<HatsContainer hats={hats} />);

        expect(component.getByText("hat1")).toBeInTheDocument();
        expect(component.getByText("hat2")).toBeInTheDocument();
        expect(component.getByText("hat3")).toBeInTheDocument();
    });

    it("should render with active class if active", () => {
        component = render(<HatsContainer hats={hats} />);

        expect(component.getByText("hat1")).toHaveClass("active");
        expect(component.getByText("hat2")).not.toHaveClass("active");
        expect(component.getByText("hat3")).toHaveClass("active");
    });
});
