import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { ISkillRef } from "../services/SkillsService";
import SkillsContainer from "./SkillsContainer";
import ItemService from "../services/ItemsService";
import IItem from "../interfaces/IItem";
import { makeItem } from "../setupTests";

describe("SkillsContainer", () => {
    let component:RenderResult;

    const skill1:ISkillRef = {
        name: "skill1",
        skill: 1,
        ref: React.createRef()
    };

    const skill2:ISkillRef = {
        name: "skill2",
        skill: 2,
        ref: React.createRef()
    };

    const skill3:ISkillRef = {
        name: "skill3",
        skill: 3,
        ref: React.createRef()
    };

    const skills = [skill1, skill2, skill3];

    const item1:IItem = makeItem("item1"),
          item2:IItem = makeItem("item2"),
          item3:IItem = makeItem("item3");

    const items = [item1, item2, item3];

    const itemService = new ItemService(items);

    it("should render with the right skills", () => {
        component = render(<SkillsContainer activeItem={itemService.getActiveItem()} itemService={itemService} skills={skills} />);

        expect(component.getByAltText("skill1")).toBeInTheDocument();
        expect(component.getByAltText("skill2")).toBeInTheDocument();
        expect(component.getByAltText("skill3")).toBeInTheDocument();
    });
});
