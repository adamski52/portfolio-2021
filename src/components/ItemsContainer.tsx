import React from "react";
import IItem from "../interfaces/IItem";
import Item from "./Item";
import "./ItemsContainer.scss";

export interface IItemsContainerProps {
    items: IItem[];
    activeItem: IItem;
}

export interface IItemsContainerState {
    items: IItem[];
    activeItem: IItem;
}

export default class ItemsContainer extends React.Component<IItemsContainerProps, IItemsContainerState> {
    constructor(props:IItemsContainerProps) {
        super(props);
        
        this.state = {
            items: props.items,
            activeItem: props.activeItem
        };
    }

    public render() {
        return (
            <div className="items-container">
                {this.renderItems()}
            </div>
        );
    }

    private renderItems() {
        return this.state.items.filter((item, index) => {
            return index < 1;
        }).map((item) => {
            return (
                <Item item={item} key={item.name} isActive={item === this.state.activeItem} />
            );
        });
    }

    public static getDerivedStateFromProps(props:IItemsContainerProps, state:IItemsContainerState)  {
        return {
            ...state,
            items: props.items,
            activeItem: props.activeItem
        };
    }
}