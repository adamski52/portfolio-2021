import React from "react";
import "./App.scss";
import HatsContainer from "../components/HatsContainer";
import SectionsContainer, { TSection } from "../components/SectionsContainer";
import SkillsContainer from "../components/SkillsContainer";
import IItem from "../interfaces/IItem";
import ILine from "../interfaces/ILine";
import HatsService, { IHat } from "../services/HatsService";
import HistoryService, { IHistory } from "../services/HistoryService";
import ItemService from "../services/ItemsService";
import SkillsService, { ISkillRef } from "../services/SkillsService";
import LinesContainer from "./LinesContainer";
import LineService, { LineDirection, PositionedHTMLElement } from "../services/LineService";
import ItemsContainer from "./ItemsContainer";

export interface IAppProps {
    hatsService: HatsService;
    skillsService: SkillsService;
    historyService: HistoryService;
    itemService: ItemService;
    lineService: LineService;
};

export interface IAppState {
    currentSection: TSection;

    items: IItem[];
    activeItem: IItem;
    lines: ILine[];

    skills: ISkillRef[];
    hats: IHat[];
    history: IHistory[];

    firstHalfSkills: ISkillRef[];
    secondHalfSkills: ISkillRef[];

    width: number;
    height: number;
};

export default class App extends React.Component<IAppProps, IAppState> {
    constructor(props: IAppProps) {
        super(props);

        let skills = this.props.skillsService.skills,
            numFirstHalfSkills = Math.ceil(skills.length / 2),
            firstHalfSkills = skills.slice(0, numFirstHalfSkills),
            secondHalfSkills = skills.slice(numFirstHalfSkills, skills.length);

        this.state = {
            currentSection: "pleasure",

            items: this.props.itemService.items,
            activeItem: this.props.itemService.getActiveItem(),
            lines: [],

            skills: skills,
            hats: this.props.hatsService.hats,
            history: this.props.historyService.history,

            firstHalfSkills: firstHalfSkills,
            secondHalfSkills: secondHalfSkills,

            width: window.innerWidth,
            height: window.innerHeight
        };

        this.onChangeSection = this.onChangeSection.bind(this);
    }

    private onChangeSection(section: TSection) {
        this.setState({
            currentSection: section
        });
    }

    public render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <SectionsContainer
                            currentSection={this.state.currentSection}
                            onChangeSection={this.onChangeSection}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-1 align-self-center text-center">
                        <SkillsContainer activeItem={this.state.activeItem} skills={this.state.firstHalfSkills} itemService={this.props.itemService} />
                    </div>
                    <div className="col-10 align-self-center text-center">
                        <ItemsContainer activeItem={this.state.activeItem} items={this.state.items} />
                    </div>
                    <div className="col-1 align-self-center text-center">
                        <SkillsContainer activeItem={this.state.activeItem} skills={this.state.secondHalfSkills} itemService={this.props.itemService} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <HatsContainer hats={this.state.hats} />
                    </div>
                </div>
                <LinesContainer lines={this.state.lines} width={this.state.width} height={this.state.height} lineThickness={this.props.lineService.lineWidth} />
            </div>
        );
    }


    private figureLines() {
        let lines:ILine[] = [],
            activeItem:PositionedHTMLElement = {
                height: this.state.height,
                width: this.state.width,
                left: 0,
                top: 0
            };
        
        if(!activeItem) {
            return lines;
        }

        this.state.skills.forEach((skill) => {
            let rect = skill.ref.current!.getBoundingClientRect(),
                skillElement = {
                    top: rect.top,
                    left: rect.left,
                    height: rect.height,
                    width: rect.width
                },
                linePosition = this.props.lineService.getLinePosition(activeItem!, skillElement, LineDirection.HORIZONTAL);

            lines.push({
                ...linePosition,
                color: "#000"
            });
        });

        return lines;
    }

    public componentDidMount() {
        window.addEventListener("resize", () => {
            this.setState({
                width: window.innerWidth,
                height: window.innerHeight,
                lines: this.figureLines()
            });
        });

        window.addEventListener("wheel", (e: WheelEvent) => {
            e.preventDefault();

            let items = this.props.itemService.scroll(e.deltaY),
                activeItem = this.props.itemService.getActiveItem();

            this.setState({
                items: items,
                activeItem: activeItem,
                lines: this.figureLines()
            });
        });

        this.setState({
            lines: this.figureLines()
        }, () => {
            console.log("set state initially: ", this.state);
        });
    }
}