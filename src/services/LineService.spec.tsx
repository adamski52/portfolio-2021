import LineService, { LineDirection } from "./LineService"

describe("LineService", () => {
    it("should use the provided lineWidth", () => {
        let lineService = new LineService(123);
        let actual = lineService.lineWidth;
        expect(actual).toEqual(123);
    });

    it("should get the horizontal line info given a starting and ending point", () => {
        let lineService = new LineService(5),
            startingElement = {
                height: 100,
                top: 200,
                left: 300,
                width: 400
            },
            endingElement = {
                height: 500,
                top: 600,
                left: 700,
                width: 800
            };

        let actual = lineService.getLinePosition(startingElement, endingElement, LineDirection.HORIZONTAL);
        expect(actual).toEqual({
            controlLeft1: 1100,
            controlTop1: 250,
            controlLeft2: 500,
            controlTop2: 850,
            startLeft: 500,
            startTop: 250,
            endLeft: 1100,
            endTop: 850
        });
    });

    it("should get the horizontal line info given a starting and ending point", () => {
        let lineService = new LineService(5),
            startingElement = {
                height: 100,
                top: 200,
                left: 300,
                width: 400
            },
            endingElement = {
                height: 500,
                top: 600,
                left: 700,
                width: 800
            };

        let actual = lineService.getLinePosition(startingElement, endingElement, LineDirection.VERTICAL);
        expect(actual).toEqual({
            controlLeft1: 500,
            controlTop1: 850,
            controlLeft2: 1100,
            controlTop2: 250,
            startLeft: 500,
            startTop: 250,
            endLeft: 1100,
            endTop: 850
        });
    });
});