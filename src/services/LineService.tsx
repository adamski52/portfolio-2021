import ILinePosition from "../interfaces/ILinePosition";

export enum LineDirection {
    VERTICAL,
    HORIZONTAL
};

export interface PositionedHTMLElement {
    top: number;
    left: number;
    width: number;
    height: number;
}

export default class LineService {
    private _lineWidth:number = 1;
    constructor(lineWidth:number) {
        this._lineWidth = lineWidth;
    }

    public get lineWidth() {
        return this._lineWidth;
    }

    private getLeft(element: PositionedHTMLElement) {
        return element.left + (element.width / 2);
    }

    private getTop(element: PositionedHTMLElement) {
        return element.top + (element.height / 2);
    }

    private getControlPoints(startingPoint:number[], endingPoint:number[], direction:LineDirection) {
        if(direction === LineDirection.VERTICAL) {
            return [
                [startingPoint[0], endingPoint[1]],
                [endingPoint[0], startingPoint[1]]
            ];
        }

        return [
            [endingPoint[0], startingPoint[1]],
            [startingPoint[0], endingPoint[1]]
        ];
    }

    public getLinePosition(startingElement:PositionedHTMLElement, endingElement:PositionedHTMLElement, direction:LineDirection):ILinePosition {
        let startLeft = this.getLeft(startingElement),
            startTop = this.getTop(startingElement);

        let endLeft = this.getLeft(endingElement),
            endTop = this.getTop(endingElement);

        let startingPoint = [startLeft, startTop],
            endingPoint = [endLeft, endTop];

        let controlPoints = this.getControlPoints(startingPoint, endingPoint, direction);
            
        return {
            controlLeft1: controlPoints[0][0],
            controlTop1: controlPoints[0][1],
            controlLeft2: controlPoints[1][0],
            controlTop2: controlPoints[1][1],
            startTop: startTop,
            startLeft: startLeft,
            endTop: endTop,
            endLeft: endLeft
        };
    }
}