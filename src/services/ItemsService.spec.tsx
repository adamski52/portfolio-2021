import IItem from "../interfaces/IItem";
import { makeItem } from "../setupTests";
import ItemsService from "./ItemsService";

const item1:IItem = makeItem("item1"),
      item2:IItem = makeItem("item2"),
      item3:IItem = makeItem("item3");

let itemsService:ItemsService;
describe("ItemsService", () => {
    beforeEach(() => {
        itemsService = new ItemsService([item1, item2, item3]);
    });

    it("should gimme", () => {
        let actual = itemsService.items;
        expect(actual.length).toEqual(3);
    });

    it("should scroll backward", () => {
        let actual = itemsService.scroll(-1);

        expect(actual).toEqual([
            item2,
            item3,
            item1
        ]);
    });

    it("should scroll forward", () => {
        let actual = itemsService.scroll(1);

        expect(actual).toEqual([
            item3,
            item1,
            item2
        ]);
    });

    it("should scroll -1 even if < -1", () => {
        let actual = itemsService.scroll(-99);

        expect(actual).toEqual([
            item2,
            item3,
            item1
        ]);
    });

    it("should scroll 1 even if > 1", () => {
        let actual = itemsService.scroll(99);

        expect(actual).toEqual([
            item3,
            item1,
            item2
        ]);
    });

    it("should scroll 1 even if 0", () => {
        let actual = itemsService.scroll(0);

        expect(actual).toEqual([
            item3,
            item1,
            item2
        ]);
    });

    it("should gimme the 0th item as active even after scrolling", () => {
        let actual = itemsService.getActiveItem();
        expect(actual).toEqual(item1);

        itemsService.scroll(1);
        actual = itemsService.getActiveItem();
        expect(actual).toEqual(item3);

        itemsService.scroll(-1);
        itemsService.scroll(-1);
        actual = itemsService.getActiveItem();
        expect(actual).toEqual(item2);
    });

    it("should return true if a skill is used in an item", () => {
        let actual = itemsService.isSkillUsedInItem(item1, "item1-1");
        expect(actual).toEqual(true);
    });

    it("should return false if a skill is not used in an item", () => {
        let actual = itemsService.isSkillUsedInItem(item1, "mind control");
        expect(actual).toEqual(false);
    });
});