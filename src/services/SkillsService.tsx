import React, { RefObject } from "react";

export interface ISkill {
    name: string;
    skill: number;
}

export interface ISkillRef extends ISkill {
    ref: RefObject<HTMLImageElement>;
}

export default class SkillsService {
    private _skills:ISkillRef[] = [{
        name: "actionscript",
        skill: 90
    }, {
        name: "angular",
        skill: 90
    }, {
        name: "apache",
        skill: 50
    }, {
        name: "aws",
        skill: 80
    }, {
        name: "backbonejs",
        skill: 70
    }, {
        name: "bash",
        skill: 80
    }, {
        name: "blender",
        skill: 10
    }, {
        name: "bootstrap",
        skill: 90
    }, {
        name: "camel",
        skill: 70
    }, {
        name: "chai",
        skill: 90
    }, {
        name: "csharp",
        skill: 70
    }, {
        name: "css",
        skill: 90
    }, {
        name: "cucumber",
        skill: 80
    }, {
        name: "django",
        skill: 60
    }, {
        name: "docker",
        skill: 85
    }, {
        name: "dotnet",
        skill: 60
    }, {
        name: "express",
        skill: 80
    }, {
        name: "fhir",
        skill: 90
    }, {
        name: "flash",
        skill: 90
    }, {
        name: "git",
        skill: 90
    }, {
        name: "github",
        skill: 90
    }, {
        name: "gitlab",
        skill: 90
    }, {
        name: "go",
        skill: 50
    }, {
        name: "gradle",
        skill: 70
    }, {
        name: "grafana",
        skill: 70
    }, {
        name: "hazelcast",
        skill: 80
    }, {
        name: "helm",
        skill: 80
    }, {
        name: "html",
        skill: 100,
    }, {
        name: "ignite",
        skill: 80
    }, {
        name: "illustrator",
        skill: 90
    }, {
        name: "jasmine",
        skill: 100
    }, {
        name: "java",
        skill: 90
    }, {
        name: "javascript",
        skill: 100
    }, {
        name: "jenkins",
        skill: 90
    }, {
        name: "jest",
        skill: 90
    }, {
        name: "jira",
        skill: 90
    }, {
        name: "jquery",
        skill: 90
    }, {
        name: "junit",
        skill: 90
    }, {
        name: "kubernetes",
        skill: 85
    }, {
        name: "kafka",
        skill: 85
    }, {
        name: "karma",
        skill: 85
    }, {
        name: "less",
        skill: 90
    }, {
        name: "maven",
        skill: 90
    }, {
        name: "micronaut",
        skill: 80
    }, {
        name: "mocha",
        skill: 90
    }, {
        name: "mockito",
        skill: 90
    }, {
        name: "mongo",
        skill: 75
    }, {
        name: "mssql",
        skill: 85
    }, {
        name: "mysql",
        skill: 85
    }, {
        name: "nginx",
        skill: 75
    }, {
        name: "nifi",
        skill: 90
    }, {
        name: "nodejs",
        skill: 100
    }, {
        name: "npm",
        skill: 100
    }, {
        name: "photoshop",
        skill: 90
    }, {
        name: "pixijs",
        skill: 75
    }, {
        name: "postgres",
        skill: 85
    }, {
        name: "postman",
        skill: 95
    }, {
        name: "prometheus",
        skill: 70
    }, {
        name: "python",
        skill: 75
    }, {
        name: "react",
        skill: 100
    }, {
        name: "redux",
        skill: 95
    }, {
        name: "sam",
        skill: 80
    }, {
        name: "sass",
        skill: 100
    }, {
        name: "selenium",
        skill: 70
    }, {
        name: "sinon",
        skill: 85
    }, {
        name: "springboot",
        skill: 85
    }, {
        name: "tableau",
        skill: 90
    }, {
        name: "terraform",
        skill: 65
    }, {
        name: "travis",
        skill: 70
    }, {
        name: "typescript",
        skill: 100
    }, {
        name: "unity",
        skill: 60
    }, {
        name: "vue",
        skill: 65
    }].map((skill) => {
        return {
            ...skill,
            ref: React.createRef()
        };
    });

    public get skills():ISkillRef[] {
        return this._skills;
    }

    public getSkillsByName(...names: string[]) {
        let matches:ISkill[] = [];
        
        names.map((name) => {
            return name.toLowerCase();
        }).filter((name, index) => {
            return names.indexOf(name) === index;
        }).forEach((name) => {
            name = name.toLowerCase();
            let skill = this.skills.find((skill) => {
                return skill.name.toLowerCase() === name;
            });

            if(skill) {
                matches.push(skill);
            }
        });

        return matches;
    }
}