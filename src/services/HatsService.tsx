export interface IHat {
    name: string;
    isActive: boolean;
}

export default class HatsService {
    private _hats = [{
        name: "development",
        isActive: false
    }, {
        name: "design",
        isActive: false
    }, {
        name: "leadership",
        isActive: false
    }, {
        name: "mentor",
        isActive: false
    }];

    public get hats() {
        return this._hats;
    }

    public getHatsByName(...names: string[]) {
        let matches:IHat[] = [];
        
        names.map((name) => {
            return name.toLowerCase();
        }).filter((name, index) => {
            return names.indexOf(name) === index;
        }).forEach((name) => {
            name = name.toLowerCase();
            let hat = this.hats.find((hat) => {
                return hat.name.toLowerCase() === name;
            });

            if(hat) {
                matches.push(hat);
            }
        });

        return matches;
    }
}