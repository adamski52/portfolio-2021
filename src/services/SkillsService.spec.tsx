import React from "react";
import SkillsService from "./SkillsService";

describe("SkillsService", () => {
    const skillsService = new SkillsService();
    const skillRef = React.createRef();
    it("should gimme skills", () => {
        expect(skillsService.skills.length).toEqual(72);
    });

    it("should gimme skills by name", () => {
        let actual = skillsService.getSkillsByName("actionscript", "javascript", "nodejs");
        expect(actual).toEqual([{
            name: "actionscript",
            skill: 90,
            ref: skillRef
        }, {
            name: "javascript",
            skill: 100,
            ref: skillRef
        }, {
            name: "nodejs",
            skill: 100,
            ref: skillRef
        }]);
    });

    it("should remove dupes from list of skills (and be case insensitive about it)", () => {
        let actual = skillsService.getSkillsByName("actionscript", "ActionScript", "javascript", "JavaScript")
        expect(actual).toEqual([{
            name: "actionscript",
            skill: 90,
            ref: skillRef
        }, {
            name: "javascript",
            skill: 100,
            ref: skillRef
        }]);
    });
});