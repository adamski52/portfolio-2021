import IItem from "../interfaces/IItem";

export default class ItemService {
    private _items:IItem[];

    constructor(items:IItem[]) {
        this._items = items;
    }

    public get items() {
        return this._items;
    }

    public scroll(direction:number) {
        if(!this.items) {
            return this.items;
        }

        direction = direction >= 0 ? 1 : -1;

        if(direction === 1) {
            this.items.unshift(this.items.pop() as IItem);
            return this.items;
        }

        this.items.push(this.items.shift() as IItem);
        return this.items;
    }

    public getActiveItem() {
        return this.items[0];
    }

    public isSkillUsedInItem(item:IItem, skill:string) {
        return item.tags.indexOf(skill.toLowerCase()) > -1;
    }
}