import HatsService from "./HatsService";

describe("HatsService", () => {
    const hatsService = new HatsService();

    it("should gimme hats", () => {
        expect(hatsService.hats.length).toEqual(4);
    });

    it("should gimme hats by name", () => {
        let actual = hatsService.getHatsByName("mentor", "development");
        expect(actual).toEqual([{
            name: "mentor",
            isActive: false
        }, {
            name: "development",
            isActive: false
        }]);
    });

    it("should remove dupes from list of skills (and be case insensitive about it)", () => {
        let actual = hatsService.getHatsByName("mentor", "Mentor", "development", "Development")
        expect(actual).toEqual([{
            name: "mentor",
            isActive: false
        }, {
            name: "development",
            isActive: false
        }]);
    });
});