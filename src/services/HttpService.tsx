import fetch from "cross-fetch";

export default class HttpService {
    private baseUrl = "";

    constructor(baseUrl:string) {
        this.baseUrl = baseUrl;
    }

    private toJson(response:Response) {
        if(response.ok) {
            return response.json();
        }

        throw response;
    }

    public async get(url:string) {
        let response = await fetch(this.baseUrl + url, {
            headers: {
                "Content-Type": "application/json"
            }
        });
        
        return this.toJson(response);
    }
}
