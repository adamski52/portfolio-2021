import HatsService from "./HatsService";
import HistoryService from "./HistoryService";
import SkillsService from "./SkillsService"

describe("HistoryService", () => {
    const skillsService = new SkillsService();
    const hatsService = new HatsService();
    const historyService = new HistoryService(skillsService, hatsService);

    it("should gimme", () => {
        let actual = historyService.history;
        expect(actual.length).toEqual(1);
    });
});