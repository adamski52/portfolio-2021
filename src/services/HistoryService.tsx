import HatsService, { IHat } from "./HatsService";
import SkillsService, { ISkill } from "./SkillsService";

export interface IHistory {
    company: string;
    title: string;
    startDate: string;
    endDate: string;
    skills: ISkill[];
    hats: IHat[];
    description: string[];
    logoUrl?: string;
}

export default class HistoryService {
    private skillsService:SkillsService;
    private hatsService:HatsService;
    private _history:IHistory[];

    constructor(skillsService:SkillsService, hatsService:HatsService) {
        this.skillsService = skillsService;
        this.hatsService = hatsService;
        
        this._history = [{
            company: "TEKSystems",
            title: "Agile/Technical Coach",
            startDate: "Aug 10, 2020",
            endDate: "present",
            skills: this.skillsService.getSkillsByName("javascript", "typescript", "react", "java", "springboot", "micronaut", "junit", "jest", "maven", "gradle", "aws", "terraform", "helm", "confluence", "jira", "github", "git", "nodejs", "python", "go"),
            hats: this.hatsService.getHatsByName("development", "mentor"),
            description: ["i did gud"]
        }];
    }

    public get history() {
        return this._history;
    }
}