import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./components/App";
import ItemService from "./services/ItemsService";
import HatsService from "./services/HatsService";
import HistoryService from "./services/HistoryService";
import SkillsService from "./services/SkillsService";
import HttpService from "./services/HttpService";
import LineService from "./services/LineService";


(async () => {
  const hatsService = new HatsService();
  const skillsService = new SkillsService();
  const historyService = new HistoryService(skillsService, hatsService);
  const httpService = new HttpService("");
  const itemService = new ItemService(await httpService.get("/api/items.json"));
  const lineService = new LineService(1);

  ReactDOM.render(
    <React.StrictMode>
      <App 
        lineService={lineService}
        itemService={itemService}
        hatsService={hatsService}
        skillsService={skillsService}
        historyService={historyService}
      />
    </React.StrictMode>,
    document.getElementById("root")
  );
})();